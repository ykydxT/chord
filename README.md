COMP90020 Distributed Algorithm Project
=======================================

General Description
-------------------
This application is a simplified Distributed File System(DFS), based on the concept and algorithm of Chord DHT.

The application runs on localhost.

A server ring can be formed by one or more nodes. Each node would have its own hash value(key) by applying SHA256 hash function to its own port number.
The order of nodes in the ring is determined by their unique key. Each node stores the key and address of its successor. If nodes leave the ring, the
server still can operate properly as long as at least one node is working.

Client can connect to any existing node and make some file operation on the server ring(save, remove, get, list). The node where the file is stored is
decided by the hash value of file.

If one node leaves, it would pass its file to its successor and inform its predecessor to have a new successor.

Out application provides GUI.

How to run the code
-------------------
_Server

To run the first node:
#python node.py [port number]                                           
e.g. 
#python node.py 9200

To add a node into the existing ring:
#python node.py [port number] [port number of a existing node]         
e.g. 
#python node.py 9201 9200

To interrupt a node:
Ctrl + c

_Client
To run a client:
#python client.py [client ip:port number] [node ip:port number]                                          e.g. 
#python node.py 9200

After entering the terminal:
To upload(set) any file:
#-s or set [file name]

To search any file:
#-g or get [file hash]

To remove any file:
#-rm or remove [file hash]

To list all files:
#-ls or list

To exit:
#exit


Python version
--------------
Python 3.5

Package installed
-----------------
zmq
hashlib
base64
PyQt5  

Author
-----------------
Dexiao Ye
Bin Li
Tianxiong Han
Chi Che