import msg_tool
import time
import json

default_ip = "localhost:"


def file_to_node(node, file_id, file_name, file_data):
    node["files"][file_id] = {"name": file_name, "data": file_data}


def remove_file_node(node, file_id):
    del node["files"][file_id]


def check_file(node, file_id):
    for file in node:
        if file == file_id:
            return node[file]
            break
    return "No"


def check_rank(my_id, suc_id, new_id):
    if suc_id > my_id:
        if new_id > suc_id or new_id < my_id:
            return True
        else:
            return False
    else:
        if (my_id >= new_id > suc_id) or (my_id == suc_id):
            return True
        else:
            return False


def add(node, req, socket_send):
    check_in = check_rank(node["id"], node["suc_id"], req["msg"]["id"])
    print(node["id"], node["suc_id"], req["msg"]["id"])
    # bewteen  successor and me
    if check_in:
        print("Do not push")
        req_upd = msg_tool.create_req("update", node["port"], req["msg"]["origin"],
                                      {"suc_id": node["suc_id"], "suc_port": node["suc_port"]})
        print(req["msg"]["origin"])
        msg_tool.send(req_upd)
        time.sleep(1)
        socket_send.connect('tcp://' + default_ip + req_upd["to"])
        socket_send.send_json(req_upd)
        msg = socket_send.recv()
        msg_tool.recv(msg)

        node["suc_id"] = req["msg"]["id"]
        node["suc_port"] = req["msg"]["origin"]

        msg_tool.show_state(node)

    else:
        print("Push to my successor")
        req_add = msg_tool.create_req(
            'add', node["port"], node["suc_port"],
            {'origin': req["msg"]["origin"],
             'id': req["msg"]["id"]})
        socket_send.connect('tcp://' + default_ip + req_add['to'])
        socket_send.send_json(req_add)
        msg_tool.send(req_add)
        msg = socket_send.recv()
        msg_tool.recv(msg)


def update(node, req):
    node["suc_id"] = req["msg"]["suc_id"]
    node["suc_port"] = req["msg"]["suc_port"]
    print("Updated")
    msg_tool.show_state(node)


def operation_file(node, req, socket_send, operation):
    respond_oper = "empty"
    if operation == "save":
        check_fi = check_rank(node["id"], node["suc_id"], req["id"])
        if check_fi:
            print("Save the file locally")
            file_to_node(node, req["id"], req["name"], req["data"])
            msg_tool.show_state(node)
            ''''
              elif operation == "remove":
                 remove_file_node(node, req['id'])
           '''
        else:
            print("Push the save operation to my successor")
            respond_oper = msg_tool.create_req_data("save",  node["port"],
                                      node["suc_port"], req["id"]
                                     , req["name"], req["data"])
        #elif operation == "remove":
            #respond_oper = msg_tool.create_req("remove",  node["port"],
            #                            node["suc_port"], req["id"])
           # respond_oper = json.dumps({
           #     "req": "remove", "from": node["port"], "to":  node["suc_port"], "id": req["id"],
            #})
           # respond_oper = json.loads(respond_oper)
    elif operation == "remove":
        if check_file(node["files"], req["id"]) == "No":
            if req["origin_node"] == (default_ip + node["suc_port"]):
                print("No such file to remove!")
            else:
                respond_oper = json.dumps({
                "req": "remove", "from": node["port"], "to":  node["suc_port"], "id": req["id"], "origin_node": req["origin_node"]
             })
                respond_oper = json.loads(respond_oper)

        else:
            print("Removing the file locally", req['id'])
            remove_file_node(node, req['id'])
            msg_tool.show_state(node)

    if respond_oper != "empty":
        socket_send.connect('tcp://' + default_ip + respond_oper["to"])
        socket_send.send_json(respond_oper)
        msg_tool.send(respond_oper)
        msg = socket_send.recv()
        msg_tool.recv(msg)


def get_file(node, req, socket_send):
    check = check_file(node["files"], req["id"])
    if check != "No":
        print("I got the requested file! Its id content ", check)
        req_send_json = msg_tool.create_req_get(node["port"], req["client_origin"], check)
        socket_send.connect('tcp://' + req_send_json["to"])
        print('tcp://' + req_send_json["to"])
        socket_send.send_json(req_send_json)
        msg_tool.send(req_send_json)
        msg = socket_send.recv()
        msg_tool.recv(msg)
    else:
        print("I do not have the file, I am gonna ask my successor")
        if req["node_origin"] == (default_ip + node["suc_port"]):
            req_send_json = msg_tool.create_req_get(node["port"], req["client_origin"], "No")
            socket_send.connect('tcp://' + req_send_json["to"])
            socket_send.send_json(req_send_json)
            msg_tool.send(req_send_json)
            msg = socket_send.recv()
            msg_tool.recv(msg)
        else:
            get_req = json.dumps({
                "req": "get", "from": req["from"], "to": node["suc_port"], "id": req["id"],
                "node_origin": req["node_origin"], "client_origin": req["client_origin"]
            })
            get_req_json = json.loads(get_req)
            socket_send.connect('tcp://' + default_ip + get_req_json["to"])
            socket_send.send_json(get_req_json)
            msg_tool.send(get_req_json)
            msg = socket_send.recv()
            msg_tool.recv(msg)


def pass_data(node, req, socket_send):
    for file in req["msg"]:
        node["files"][file] = req["msg"][file]
    if node["suc_port"] == req["from"]:
        print("My successor is leaving and I am the last node. I feel so lonely")
        node["suc_port"] = node["port"]
        node["suc_id"] = node["id"]
        msg_tool.show_state(node)
    else:
        msg_tool.show_state(node)
        req_json = msg_tool.create_req_leave("help_leave", node["port"], node["suc_port"], req["from"], node["port"], node["id"])
        socket_send.connect('tcp://' + default_ip + req_json["to"])
        socket_send.send_json(req_json)
        msg_tool.send(req_json)
        msg = socket_send.recv()
        msg_tool.recv(msg)


def help_leave(node, req, socket_send):
    if node["suc_port"] == req["origin"]:
        print("My successor is leaving and I am gonna have a new successor")
        node["suc_port"] = req["origin_suc"]
        node["suc_id"] = req["origin_suc_id"]
        msg_tool.show_state(node)
    else:
        req_json = msg_tool.create_req_leave("help_leave", node["port"], node["suc_port"], req["origin"], req["origin_suc"], req["origin_suc_id"])
        socket_send.connect('tcp://' + default_ip + req_json["to"])
        socket_send.send_json(req_json)
        msg_tool.send(req_json)
        msg = socket_send.recv()
        msg_tool.recv(msg)



