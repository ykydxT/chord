import zmq
import sys
import hashlib
import json
import msg_tool
import ring
import time
default_ip = "localhost:"
node = {"ip": "localhost", "port": " ", "id": " ", "suc_id": " ", "suc_port": " ", "files": {}}
context = zmq.Context()
socket = context.socket(zmq.REP)
socket_send = context.socket(zmq.REQ)


def get_id(my_port):
    my_id = sha_256(my_port)
    node["id"] = my_id

    print("My id:", my_id)


def sha_256(original_data):
    return hashlib.sha256(str(original_data).encode('utf-8')).hexdigest()


def update_suc(other_port):
    if other_port:
        print("Other nodes exits, getting my successor")
        req_add = msg_tool.create_req(
            "add", node["port"], other_port, {"origin": node["port"], "id": node["id"]})
        socket_send.connect('tcp://' + default_ip + req_add['to'])
        print('tcp://' + default_ip + req_add['to'])
        socket_send.send_json(req_add)
        msg_tool.send(req_add)
        recv_msg = socket_send.recv()
        msg_tool.recv(recv_msg)
    else:
        node["suc_id"] = node["id"]
        node["suc_port"] = node["port"]


def main():
    global node, socket
    len_argv = len(sys.argv)
    other_port = ""
    if len_argv >= 2:
        if len_argv == 2:
            print("I am the first node!")
        my_port = sys.argv[1]
        print("I am:", my_port)
        node['port'] = my_port
        socket.bind('tcp://*:' + my_port)
        get_id(my_port)
        if len_argv == 3:
            other_port = sys.argv[2]
        update_suc(other_port)
        try:
            while 1:
                print("Waiting for request...")
                msg = socket.recv()
                recv_json = json.loads(str(msg).lstrip("b'").rstrip("'"))
                socket.connect("tcp://" + default_ip + recv_json["from"])
                recv_type = recv_json["req"]
                msg_tool.recv(recv_json)
                if recv_type == "add":
                    print("Adding a new node")
                    socket.send_string(node['port'] + "received add request")
                    socket_send = context.socket(zmq.REQ)
                    ring.add(node, recv_json, socket_send)
                elif recv_type == "update":
                    print("Updating my successor")
                    socket.send_string(node["port"] + " recieive the update")
                    ring.update(node, recv_json)
                elif recv_json["req"] == "save":
                    print("Saving a new file")
                    socket_send = context.socket(zmq.REQ)
                    socket.send_string(node["port"] + " receiving the file")
                    ring.operation_file(node, recv_json, socket_send, "save")
                elif recv_json["req"] == 'remove':
                    print('Removing a file')
                    socket_send = context.socket(zmq.REQ)
                    socket.send_string(node["port"] + " removing the file")
                    ring.operation_file(node, recv_json, socket_send, "remove")
                elif recv_json["req"] == "get":
                    print('Getting the file')
                    socket_send = context.socket(zmq.REQ)
                    socket.send_string(node["port"] + " getting the file")
                    ring.get_file(node, recv_json, socket_send)
                elif recv_json["req"] == "pass_data":
                    socket_send = context.socket(zmq.REQ)
                    socket.send_string(node["port"] + " getting the leaving message")
                    ring.pass_data(node, recv_json, socket_send)
                elif recv_json["req"] == "help_leave":
                    socket_send = context.socket(zmq.REQ)
                    socket.send_string(node["port"] + " getting the help leaving message")
                    ring.help_leave(node, recv_json, socket_send)

        except KeyboardInterrupt:
            if node["port"] != node["suc_port"]:
                leave_req = msg_tool.create_req("pass_data", node["port"], node["suc_port"], node["files"])
                socket_send = context.socket(zmq.REQ)
                socket_send.connect('tcp://' + default_ip + leave_req["to"])
                socket_send.send_json(leave_req)
                msg_tool.send(leave_req)
                time.sleep(1)
                msg = socket_send.recv()
                msg_tool.recv(msg)
            else:
                print("THE END")

    else:
        print("Input error")


if __name__ == '__main__':
    main()
