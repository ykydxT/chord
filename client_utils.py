import base64,json,sys,hashlib,os


def toHex(data):
    return base64.b16encode(data.encode())


def hexToDec(data):
    return base64.b16decode(data)


def client_data(data):
    for i in data:
        print(i)


def client_info(client):
    print('################################################')
    print('Client IP:' + client['ip'] + ':' + client['port'])
    print('Files:')
    data=client['data']
    for file_id in data:
        print("|___",data[file_id]["name"],file_id)
    print()
    print('################################################')


def options():
    print('###########################################################')
    print('Options')
    print('exit                      ->    Close client connection')
    print('-g or get <id>            ->    Get file')
    print('-h, help                  ->    Get help')
    print('ls                        ->    List of my files in DHT')
    print('-rm or remove <id>        ->    Remove file from the System')
    print('-s or set <filename>      ->    Upload a file')
    print('###########################################################')

def list_file(client):
    for i in client['data']:
        print(client['data'][i]['name']+':', i)


# def printJSON(varJSON):
#     print(json.dumps(varJSON, indent=2, sort_keys=True))


def clear():
    sys.stderr.write("\x1b[2J\x1b[H")


def set_handler(client, node, file):
    data = get_file(file)

    if data:
        file_info = json.loads(json.dumps({
            'req': 'save',
            'from': client['ip'] + ':' + client['port'],
            'to': node,
            'data': data.decode(),
            'name': get_filename(file),
            'id': sha256(data)
        }))

        client['data'][sha256(data)] = {
            'name': get_filename(file),
            'data': data.decode()
        }
        # client_info(client)
        # printJSON(file_info)
        return file_info
    else:
        return 'NO'


def remove_local(client, file_id):
    flag = False
    for i in client['data']:
        if i == file_id:
            flag = True
            del client['data'][i]
            break
    return flag


def remove_handler(client, to, file_id):
    flag = remove_local(client, file_id)

    if flag:
        file_info = json.loads(json.dumps({
            'req': 'remove',
            'from': client['ip'] + ':' + client['port'],
            'to': to,
            'id': file_id,
            'origin_node':to
        }))
        # client_info(client)

        # printJSON(file_info)
        return file_info
    else:
        return 'NO'


def get_handler(client, to, file_id):
    file_info = json.loads(json.dumps({
        'req': 'get',
        'from': client['ip'] + ':' + client['port'],
        'to': to,
        'id': file_id,
        'node_origin': to,
        'client_origin': client['ip'] + ':' + client['port']
    }))

    # printJSON(file_info)
    return file_info


def get_file(path):
    try:
        data = open(path, 'r').read()
    except:
        data = ''
    return toHex(data)


def get_filename(path):
    return os.path.basename(path)


def sha256(toHash):
    return str(hashlib.sha256(str(toHash).encode()).hexdigest())


def write_file(client_ip,data):
    filename=client_ip.split(":")[1]+"_Downloads/"+data["name"]
    os.makedirs(os.path.dirname(filename),exist_ok=True)
    file = open(filename, 'wb')
    file.write(hexToDec(data['data']))
    file.close()