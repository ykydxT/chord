import json,sys,zmq
import client_utils as c_util

default_ip = "localhost:"

client = json.loads(json.dumps({
    'ip': '',
    'port': '',
    'data': {}
}))

context = zmq.Context()
socket = context.socket(zmq.REP)
socket_send = context.socket(zmq.REQ)


def set_client(client_ip):
    client['ip'], client['port'] = client_ip.split(':')
    socket.bind('tcp://*:' + client['port'])

def req_conn(req):
    socket_send = context.socket(zmq.REQ)
    socket_send.connect('tcp://' + req['to'])
    socket_send.send_json(req)
    message = socket_send.recv()
    return message


def main():
    global client, socket, socket_send

    if len(sys.argv) != 3:
        print('>>>Please type correct arguments!')
        print('>>>Pattern: <Your Host> <Node Host>')
    else:
        node = sys.argv[2]
        client_ip = sys.argv[1]
        set_client(client_ip)

        # c_util.client_info(client)
        c_util.clear()
        print('>>>Welcome to CHORD!')
        print('>>>Type help or -h for more information\n')
        try:
            while True:
                print("")
                try:
                    cmd = input(client_ip+'@chord:$ ')
                    cmd = cmd.split()
                except:
                    print('>>>Exiting Terminal<<<')
                    break
                if len(cmd)>0:
                    if cmd[0] == '-h' or cmd[0] == 'help':
                        c_util.options()

                    elif cmd[0] == 'ls' or cmd[0] == 'list':
                        c_util.client_info(client)
                        # c_util.list_file(client)

                    elif cmd[0] == '-rm' or cmd[0] == 'remove':
                        if len(cmd)==2:
                            remove_req = c_util.remove_handler(client, node, cmd[1])
                        else:
                            remove_req = 'NO'
                        if remove_req == 'NO':
                            print('>>>Please assign a correct file id to remove.\n')
                        else:
                            message = req_conn(remove_req)
                            if message is not None:
                                print(">>>Request Accepted.\n")
                        c_util.client_info(client)

                    elif cmd[0] == '-s' or cmd[0] == 'set':
                        if len(cmd)==2:
                            set_req = c_util.set_handler(client, node, cmd[1])
                        else:
                            set_req = 'NO'
                        if set_req == 'NO':
                            print('>>>Please enter a correct file path!\n')
                        else:
                            message = req_conn(set_req)
                            if message is not None:
                                print(">>>File accepted:",cmd[1])
                                print(">>>Hashed ID:",set_req["id"]+"\n")
                        c_util.client_info(client)

                    elif cmd[0] == '-g' or cmd[0] == 'get':
                        if len(cmd)==2:
                            get_req = c_util.get_handler(client, node, cmd[1])
                            message = req_conn(get_req)
                            if message is not None:
                            print(">>>Searching:",cmd[1])

                            message = socket.recv()
                            message = json.loads(message.decode())
                            socket.connect('tcp://' + default_ip + message['from'])
                            socket.send_string('Ack:File Received')
                        else:
                            message={'info':'No'}
                        if message['info'] == 'No':
                            print('>>>Ops! We do not have that file!')
                            print('>>>Please check your FileID again or Contact the id Provider.')
                        else:
                            print(">>>From Node:",message['from'])
                            print(">>>Downloading File.Relax!")
                            c_util.write_file(client_ip,message['info'])
                            print(">>>"+message['info']['name']+" is saved at directory:",client_ip.split(":")[1]+"_Downloads/")
                            print(">>>"+cmd[1])
                    elif cmd[0] == 'exit':
                        print('>>>Exiting Terminal<<<')
                        break
                    else:
                        print('>>>Please type a correct option!')
                        print('>>>Type help or -h for more information')
        except KeyboardInterrupt:
            print('KeyboardInterrupted\n>>>Exiting Terminal<<<')
            exit(0)

if __name__ == '__main__':
    main()