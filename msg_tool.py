import json


def create_req(req, who, to, msg):
    return json.loads(json.dumps({"req": req, "from": who, "to": to, "msg": msg}))


def create_req_data(req, who, to, msg, name, data):
    return json.loads(json.dumps({"req": req, "from": who, "to": to, "id": msg, "name": name, "data": data}))


def create_req_get(who, to, info):
    return json.loads(json.dumps({"from": who, "to": to, "info": info}))

def create_req_leave(req, who, to , origin,origin_suc, origin_suc_id):
    return json.loads(json.dumps({"req": req, "from": who, "to": to, "origin": origin, "origin_suc": origin_suc, "origin_suc_id": origin_suc_id}))

def recv(msg):
    print("********************************")
    print("Receiving:", str(msg).lstrip("b'").rstrip("'"))
    print("********************************")

def send(msg):
    print("********************************")
    print("Sending:", str(msg).lstrip("b'").rstrip("'"))
    print("********************************")

def show_state(node):
    print("********************************")
    print("The id of my successor:", node["suc_id"])
    print("The port of my successor:", node["suc_port"])
    print("File list:", node["files"])
    print("********************************")